package com.darkidiot.session.redis;

import java.util.Map;
/**
 * session attribute持久化接口
 * Copyright (c) for darkidiot
 * Date:2017/4/14
 * Author: <a href="darkidiot@icloud.com">darkidiot</a>
 * School: CUIT
 * Desc:
 */
public interface SessionSource {

    /**
     * 通过msid查找对应的session attribute
     * @param msid
     * @return
     */
    Map<String, Object> findAttributeByMsid(String msid);

    /**
     * 刷新过期时间
     * @param msid
     * @param paramInt
     */
    boolean refreshExpireTime(String msid, int paramInt);

    /**
     * 物理删除
     * @param msid
     */
    boolean deletePhysically(String msid);

    /**
     * 持久化
     * @param msid
     * @param paramMap
     * @param paramInt
     * @return
     */
    boolean save(String msid, Map<String, Object> paramMap, int paramInt);

    /**
     * 销毁
     */
    void destroy();
}
